gallery.sh
==========

[![Build Status](https://travis-ci.org/Cyclenerd/gallery_shell.svg?branch=master)](https://travis-ci.org/Cyclenerd/gallery_shell)


https://github.com/Cyclenerd/gallery_shell 'den anarcho-copy için uyarlanmıştır. 



Statik web galerileri oluşturmak için Bash Script. Sunucu taraflı programlara (yani PHP, MySQL) gerek yok.

Genel Bakış
--------
gallery.sh `convert` ve `jhead` komut satırı yardımcı programlarını kullanarak statik html thumbnail (image, photo) galerilerini üreten basit bash kabuk komut dosyasıdır.
Her şey önceden oluşturulduğundan, resim galerilerini görüntülemek için çalıştırmak için özel bir sunucu tarafı komut dosyası gerekmez.

Tek ihtiyacınız olan, düz html ve jpeg dosyalarınızı barındıracak bir yer.

Gerekli Şeyler
------------
* ImageMagick (http://www.imagemagick.org/) dönüştürme aracı için.
* JHead (http://www.sentex.net/~mwandel/jhead/) EXIF veri ayıklama için.

Debian tabanlı bir sistemde (Ubuntu), root olarak `apt-get install imagemagick jhead` komutunu çalıştırın.

Mac OS altında aşağıdaki komutlarla yükleyebilirsiniz:
* MacPort (https://www.macports.org/): `sudo port install imagemagick jhead`
* Homebrew (https://brew.sh/): `brew install imagemagick jhead`

Kullanım
-----

	gallery.sh [-t <title>] [-d <thumbdir>] [-h]:
		[-t <title>]	 başlığı belirler (varsayılan: Galeri)
		[-d <thumbdir>]	 thumbdir'i ayarlar (varsayılan: resim-dosyalari)
		[-h]		 displays help (this message)

Örnek: `gallery.sh` yada `gallery.sh -t "fotoğraflarım" -d "resimler"`

`gallery.sh` ** geçerli ** dizinde çalışır.  index.html'e bakın ve sonucu görün. 

ZIP dosyaları (.zip veya .ZIP) ve filmler (.mov, .MOV, .mp4 veya .MP4) de göz önünde bulundurulur. Galeride bir indirme düğmesi olarak görünürler.




>GNU Public License version 3.
>Please feel free to fork and modify this on GitHub (https://github.com/Cyclenerd/gallery_shell).